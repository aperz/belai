import os
from setuptools import setup, find_packages

VERSION = "0.1.0"

DEPENDENCIES = [
    "numpy",
    "scipy",
    "pandas",
    "joblib",
    "seaborn",
    "pyyaml",
    "sqlalchemy",
]

####################
# Package definition
####################

setup(
    name="belai",
    version=VERSION,
    description="",
    author="Aleksandra Perz",
    author_email="aleksandra-perz@omrf.org",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.6",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    license="AGPLv3+",

    include_package_data=False,
    packages=find_packages(),

    install_requires=DEPENDENCIES,
    #setup_requires=["pytest-runner"],
    #tests_require=["pytest"]
)
