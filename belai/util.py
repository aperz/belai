import base64
import collections
import datetime
import functools
import http.client
import logging
import os
import pathlib
import sys
import urllib.parse
import urllib.request
import zlib

import pandas as pd
import joblib
import yaml
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

#########
# Logging
#########

logging.basicConfig()
LOG = logging.getLogger("belai")
LOG.setLevel(logging.INFO)

###############
# Configuration
###############

def get_configuration():
    paths = [
        "~/.config/belai/belai.yml",
        "~/.belai.yml",
        os.path.join(os.path.dirname(__file__), "config.default.yml")
    ]
    for path in paths:
        if not os.path.exists(path):
            continue
        with open(path) as h:
            return yaml.load(h)

#############
# Memoization
#############

CACHE_ROOT = os.path.expanduser(os.path.join(get_configuration()["data"]["root"], "joblib"))
memoize = joblib.Memory(cachedir=CACHE_ROOT).cache

###########
# Downloads
###########

def _decompress_stream(input_stream, output_stream):
    """
    Decompress the contents of input_stream to output_stream (assumes GZip format).
    """
    if not isinstance(input_stream, http.client.HTTPResponse):
        assert "b" in input_stream.mode
    assert "b" in output_stream.mode

    BLOCK_SIZE = 4096
    dc = zlib.decompressobj(16+zlib.MAX_WBITS)
    while True:
        data = input_stream.read(BLOCK_SIZE)
        if not data:
            break
        output_stream.write(dc.decompress(data))

def download(url, decompress=False, expire=None, timeout=None, save_on_failure=False):
    """
    Download a file over HTTP or FTP, cache it locally, and return a path to it.

    Arguments
    ---------
    url : str
        The URL to download and cache.
    decompress : bool
        Whether to decompress the file before saving it (assumes gzip format).
    expire : numeric or None
        Expire the cache and redownload the file after this many days,
        If this parameter is None, never expire.
        If this parameter is True, force expiration and redownload.
    timeout : int, default None
        Timeout value, in seconds.
    save_on_failure : bool, default False
        If True, save an empty file to prevent repeated attempts to download
        the file.

    Returns
    -------
    A :class:`pathlib.Path` object representing the path to the downloaded file
    on the file system.
    """
    cfg = get_configuration()
    data_root = os.path.join(cfg["data"]["root"], "download")
    cache = os.path.join(os.path.expanduser(data_root))
    os.makedirs(cache, exist_ok=True)

    target_ascii = url.lower()
    if decompress:
        target_ascii += "-dc"
    target = base64.urlsafe_b64encode(target_ascii.encode("utf-8")).decode("utf-8")
    path = os.path.join(cache, target)

    expired = False

    if expire is True:
        expired = True
    elif (expire is not None) and os.path.exists(path):
        last_modified = datetime.datetime.fromtimestamp(os.stat(path).st_mtime)
        current = datetime.datetime.fromtimestamp(time.time())
        delta = (last_modified - current).days
        if delta >= expire:
            LOG.info("Download cache expired (last modified {} days ago)".format(delta))
            expired = True
    else:
        pass

    if expired or not os.path.exists(path):
        LOG.info("Downloading URL: {}".format(url))
        p = urllib.parse.urlparse(url)

        try:
            request = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
            h = urllib.request.urlopen(request)

            with open(path, "wb") as o:
                if decompress:
                    LOG.debug("Decompressing stream (decompress = True)")
                    _decompress_stream(h, o)
                else:
                    while True:
                        data = h.read(4096)
                        if len(data) == 0:
                            break
                        o.write(data)
            h.close()
            LOG.debug("URL successfully saved to: {}".format(path))
        except:
            LOG.debug("Failed to download URL: {}".format(url))
            if os.path.exists(path):
                os.unlink(path)
            if save_on_failure:
                pathlib.Path(path).touch()
                LOG.debug("(saved failure as empty file)")
            raise
    return pathlib.Path(path)

###################
# Report generation
###################

#sns.set_context("paper", font_scale=1)

def _fn_full_name(fn):
    prefix = "".join(list(map(lambda x: x[0].upper(), fn.__module__.split(".")[1:])))
    name = "-".join([prefix, fn.__name__]).replace("_", "-")
    while name.startswith("-"):
        name = name[1:]
    return name

class Make(object):
    DPI = 180
    DEFAULT_EXTENSIONS = {
        "figure": "pdf",
        "table": "tex"
    }
    PREFIX = {
        "figure": "img",
        "table": "tbl"
    }

    def __init__(self, root, extensions=None):
        self._root = root
        self._definitions = collections.OrderedDict()

        self.functions = collections.defaultdict(dict)
        self.extensions = {k:v for k,v in self.DEFAULT_EXTENSIONS.items()}
        if extensions is not None:
            for k,v in extensions.items():
                self.extensions[k] = v
        # Only table format supported for now
        assert self.extensions["table"] == "tex"

    def define(self, key, value):
        assert key.isalnum()
        self._definitions[key] = value

    def definition(self, fn):
        # FIXME: make lazy
        #name = _fn_full_name(fn)
        name = fn.__name__
        self._definitions[name] = fn()

    def figure(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(self._root, self.PREFIX["figure"],
                "{}.{}".format(name, self.extensions["figure"]))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            plt.clf()
            if not os.path.exists(path):
                fn(*args, **kwargs)
                plt.savefig(path, bbox_inches="tight", dpi=self.DPI)

        self.functions["figure"][name] = wrap
        return wrap

    def table(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(self._root, self.PREFIX["table"],
                "{}.{}".format(name, self.extensions["table"]))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            if not os.path.exists(path):
                df = fn(*args, **kwargs)
                with open(path, "w") as h:
                    df.to_latex(buf=h, index=False)

        self.functions["table"][name] = wrap
        return wrap

    def generate(self):
        for t in self.PREFIX:
            print("Generating {}s ...".format(t))
            os.makedirs(os.path.join(self._root, self.PREFIX[t]), exist_ok=True)
            for k,fn in sorted(self.functions[t].items()):
                print("*", k, file=sys.stderr)
                fn()

        if len(self._definitions) > 0:
            print("Generating latex definitions ...")
            defines_path = os.path.join(self._root, "defines.tex")
            with open(defines_path, "w") as h:
                for k,v in self._definitions.items():
                    o = "\\newcommand{\\" + str(k) + r"}{" + str(v) + "}"
                    print(o, file=h)
                    #print("\\newcommand\{\\{}\}\{{}\}".format(k, v), file=h)

make = Make("report", extensions={"figure": "png"})
