'''
.. module: example

Examples on how to use belai.

'''

import os
from belai import entity, relationship, data
from belai.config import CONFIG #config.py

data_path = os.path.join(CONFIG["ROOT_DIR"], 'data')

def biomart_data():
    """
    Retrieve BioMart data.
    """
    return data.biomart['data']

def example_gene_list(which=3):
    """
    Get an example gene list.
    """
    glist = [i.strip() for i in open(os.path.join(data_path, 'gene_list'+str(which)+'.txt')).readlines()]
    return glist

def example_EntityList(which=3):
    '''
    Get an `EntityList` object. This takes a while to compute.
    '''
    glist = [entity.Gene(g) for g in example_gene_list(which)[:6]]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    L = relationship.EntityList(glist)
    print("Genes in the list: ", L.names())
    return L

def example_important_features():
    '''
    Get the list of important features for a given `EntityList`.
    '''
    L = example_EntityList()
    return L.sel_features(n=10)

def example_bag_of_words():
    '''
    Get a bag of words for a given `EntityList`.
    '''
    L = example_EntityList()
    return L.bag_of_words(colnames = data.biomart['gene_description_columns'])





