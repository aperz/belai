"""

.. module: cheats

A utility-type stuff for testing and experimenting with features,
conveniently memoized.

"""

import os
import random
from belai import data
from belai import example
from belai import entity
from belai.util import memoize, get_configuration
from belai import relationship


config = get_configuration()
from belai.config import CONFIG #config.py


@memoize
def get_glist1():
    '''
    Random genes and SKA3
    '''
    glist = [entity.Gene(g) for g in example.example_gene_list(1)]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    return relationship.EntityList(glist)

@memoize
def get_glist2():
    '''
    Very famous genes
    '''
    glist = [entity.Gene(g) for g in example.example_gene_list(2)]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    return relationship.EntityList(glist)

@memoize
def get_glist3():
    '''
    All genes in GO term name "AP-2 adaptor complex"
    "GO term definition" : "A heterotetrameric AP-type membrane coat adaptor complex that consists of alpha, beta2, mu2 and
    sigma2 subunits, and links clathrin to the membrane surface of a vesicle, and the cargo receptors during
    receptor/clathrin mediated endocytosis. Vesicles with AP-2-containing coats are normally found primarily n
    ear the plasma membrane, on endocytic vesicles. In at least humans, the AP-2 complex can be heterogeneric
    due to the existence of multiple subunit isoforms encoded by different alpha genes (alphaA and alphaC). [G
    OC:mah, PMID:10611976, PMID:21097499, PMID:22022230, PMID:24322426]"
    '''
    glist = [entity.Gene(g) for g in example.example_gene_list(3)]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    return relationship.EntityList(glist)

@memoize
def get_glist4():
    '''
    DNA repair gene list from Gene Ontology.
    '''
    glist = [entity.Gene(g) for g in example.example_gene_list(4)]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    return relationship.EntityList(glist)

def random_gene(k):
    return [random.choice(list(set(data.biomart['data']['Gene name'])))
                for i in range(k)]

def all_genes():
    return list(set(data.biomart['data']['Gene name']))

@memoize
def all_genes_list():
    glist = [entity.Gene(g) for g in all_genes()]
    glist = [e for e in glist if e.resolved_identifier != 'Not found']
    return relationship.EntityList(glist)
