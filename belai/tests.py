import pandas as pd
from numpy import nan

from belai.config import CONFIG #config.py
import belai.crossref as cr
from belai.entity import *

def test_entity():
    g = Gene('SKA3')
    assert g.name == 'ENSG00000165480'
    assert g.resolved_identifier == "SKA3"
    assert g.resolved_colname == "Gene name"
    assert g.status == 1

    g = Gene('NOEXIST')
    assert g.direct_refs['Gene name'] = set()
    assert g.ids = set()
