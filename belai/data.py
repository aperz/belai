"""
.. module: data

Gathering data from external databases.
Memoized.

"""

import os
from pybiomart import Server
from pybiomart import Dataset
from belai.util import memoize, get_configuration

config = get_configuration()
from belai.config import CONFIG #config.py

@memoize
def biomart_gene_data():
    """
    Get data about genes from BioMart.
    """
    #GRCh38.p12
    dataset = Dataset(name='hsapiens_gene_ensembl', host='http://www.ensembl.org')
    mapp_attributes = {k:v for k,v in
        zip(dataset.list_attributes().name, dataset.list_attributes().display_name)}

    #use_attributes=dataset.list_attributes().name.dropna().tolist()
    # if too many external refs: BiomartException: Query ERROR: caught BioMart::Exception::Usage: Too many attributes selected for External References
    # TODO: recursively download and merge
    # or maybe wiki entries are problematic

    use_attributes = [
        'ensembl_gene_id', 'external_gene_name',
        'description',
        #'ccds', 'chembl', 'genedb', #'arrayexpress', 'hpa','entrezgene',
        'ensembl_transcript_id', 'ensembl_peptide_id',
        #'wikigene_id', 'wikigene_name', 'wikigene_description',

        'kegg_enzyme', #'protein_id', #'hgnc_id', 'mim_gene_accession', 'mim_gene_description',
        #'gene_biotype', 'source',
        'go_id', 'name_1006', 'definition_1006', 'go_linkage_type', 'namespace_1003',
        #'ens_hs_transcript',
        ]
    #print([mapp_attributes[a] for a in use_attributes])
    gene_hs = dataset.query(attributes=use_attributes)

    #server = Server(host='http://www.ensembl.org')
    #dataset = (server.marts['ENSEMBL_MART_ENSEMBL']
    #                 .datasets['hsapiens_gene_ensembl'])
    #dataset.query(attributes=['ensembl_gene_id', 'external_gene_name'],
    #            filters={'chromosome_name': ['1','2']})
    return gene_hs


biomart = {
    'data': biomart_gene_data(),
    #'gene_id_colname' : "Gene stable ID",
    'gene_id_colname' : "Gene name",
    'gene_id_columns' : ['Gene name', 'Gene stable ID', 'Transcript stable ID', 'Protein stable ID'],
    'gene_reference_id_columns' : ['GO term accession', 'KEGG Pathway and Enzyme ID', 'GO term name'],
    'gene_description_columns' : ['Gene description', 'GO term definition', 'GO term name'],
    'stop_words' :
            ['hgnc', 'acc', 'pmid', 'isbn', 'goc', 'mah', 'go_curators']+
            ['cell', 'cells', 'signal','group', 'activity', 'signaling', 'process', 'regulation']+
            ['binding', 'positive', 'negative', 'factor', 'response', 'increases', 'decreases']+
            ['increase', 'decrease', 'frequency', 'rate', 'extent', 'activates']+
            ['absence', 'absent', 'accompanied', 'interacting', 'change', 'non']+
            ['region', 'selectively', 'covalently', 'molecular', 'downstream', 'series']+
            ['different', 'stat', 'state', 'excludes', 'portion', 'complex', 'signals']+
            ['body', 'transcription', 'transmitting', 'combining', 'initiate', 'cellular']+
            ['internal', 'external', 'stimulus', 'chemical', 'includes', 'organism']+
            ['transduction']+
            ['receptor', 'protein', 'pathway', 'proteins', 'molecules'],
    }
