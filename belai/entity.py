'''
.. module: entity

Classed to hold information about analysed entities. And all of the matching, searching.

'''

import pandas as pd
from numpy import nan
from belai.config import CONFIG #config.py
from belai import data
import skrzynka as sk



class Entity():
    '''
    An object to hold data relating to one instance of an entity, e.g. a gene,
    a transcript, or a species.

    The barebones example of this class is a list of all IDs, including names,
    referring to the same entity.
    '''
    def __init__(self, typ, input_identifier):
        #self.names = name
        self.typ = typ
        self.input_identifier = input_identifier
        self.status = 1
        self.log = []

        self.ids = dict()
        self.direct_refs = dict()
        self.inferred_refs = dict() # well, that is IRIDESCENT

        self.change = dict()

    def add_ids(self, ids):
        """
        """
        self.ids.update(ids)

    def add_direct_refs(self, refs):
        '''
        A dictionary of 'data_source': a boolean (probability?) matrix of whether
        the Entity in question links to a specific entry in the data source.
        '''
        self.direct_refs.update(refs)


    def add_inferred_refs(self):
        """
        NI
        """
        pass

    def text_from_descriptions(self, use_cols):
        text = []
        for c in use_cols:
            text.append(", ".join(list(self.direct_refs[c])))
        text = "\n".join(text)
        return text


class Gene(Entity):
    """
    An entity of type gene.
    """
    def __init__(self, input_identifier, assume_correct_identifier=False):
        Entity.__init__(self, typ='gene', input_identifier=input_identifier)

        if not assume_correct_identifier:
            self.resolved_colname, self.resolved_identifier, self.name = self._resolve_gene_id()
        else:
            self.resolved_colname = None
            self.resolved_identifier = self.input_identifier
            self.name = self.input_identifier

        self.add_direct_refs(self.refs_biomart())



    def _resolve_gene_id(self):
        #ref_table = data.biomart_gene_data()
        #ATTENTION: flags=2 is ignore case
        x = data.biomart['data'][data.biomart['gene_id_columns']]\
                    .apply(lambda x: x.str.contains(self.input_identifier, flags=2))


        try:
            #TODO prioritize the exact match
            resolved_colname = x.sum().sort_values(ascending=True).index[-1]
            resolved_identifier = data.biomart['data'][resolved_colname][x[resolved_colname]]\
                                .value_counts().sort_values(ascending=True).index[-1]

            # Some gene names link to more than one gene ID!

            name = resolved_identifier

            return resolved_colname, resolved_identifier, name

        except IndexError:
            self.log.append("WARNING: couldn't resolve ID")
            self.status = 0
            return 'Not found', 'Not found', nan


    def refs_biomart(self):
        """
        IDs, names that point this gene in BioMart.
        """
        #TODO: for processing  many entities, it'd be better to have this table in RAM (cached ATM)
        #ref_table = data.biomart_gene_data()
        if not self.resolved_identifier == "Not found":
            x = data.biomart['data'][data.biomart['data'][data.biomart['gene_id_colname']] == self.name]
            x = {k:set(pd.Series(v).dropna()) for k,v in x.to_dict(orient='list').items()}
            return x
        else:
            return {k:set() for k in data.biomart['data'].columns}

    def bag_of_words(self):
        """
        """
        res = [list(self.direct_refs[d]) for d in data.biomart['gene_description_columns']]
        res = [[i.split(' ') for i in r ] for r in res]
        return sk.flatten(res)

    def corpus(self, colnames = "all"):
        """
        """
        if colnames == "all":
            colnames = data.biomart['gene_description_columns']
        elif not isinstance(colnames, list):
            colnames = [colnames]

        res = [list(self.direct_refs[d]) for d in colnames]
        return ". ".join(sk.flatten(res))


class Species(Entity):
    """
    An entity of type species.
    """
    def __init__(self, input_identifier):
        Entity.__init__(self, typ='species', input_identifier=input_identifier)

        self.resolved_colname, self.resolved_identifier, self.name = self._resolve_gene_id()
        self.add_direct_refs(self.refs_biomart())



    def _resolve_gene_id(self):
        #ref_table = data.biomart_gene_data()
        x = data.biomart['data'][data.biomart['gene_id_columns']]\
                    .apply(lambda x: x.str.contains(self.input_identifier))

        try:
            #TODO prioritize the exact match
            resolved_colname = x.sum().sort_values(ascending=True).index[-1]
            resolved_identifier = data.biomart['data'][resolved_colname][x[resolved_colname]]\
                                .value_counts().sort_values(ascending=True).index[-1]

            # Some gene names link to more than one gene ID!

            name = resolved_identifier

            return resolved_colname, resolved_identifier, name

        except IndexError:
            self.log.append("WARNING: couldn't resolve ID")
            self.status = 0
            return 'Not found', 'Not found', nan


    def refs_biomart(self):
        """
        """
        #TODO: for processing  many entities, it'd be better to have this table in RAM (cached ATM)
        #ref_table = data.biomart_gene_data()
        if not self.resolved_identifier == "Not found":
            x = data.biomart['data'][data.biomart['data'][data.biomart['gene_id_colname']] == self.name]
            x = {k:set(pd.Series(v).dropna()) for k,v in x.to_dict(orient='list').items()}
            return x
        else:
            return {k:set() for k in data.biomart['data'].columns}

    def bag_of_words(self):
        """
        """
        res = [list(self.direct_refs[d]) for d in data.biomart['gene_description_columns']]
        res = [[i.split(' ') for i in r ] for r in res]
        return sk.flatten(res)

    def corpus(self, colnames = "all"):
        """
        """
        if colnames == "all":
            colnames = data.biomart['gene_description_columns']
        elif not isinstance(colnames, list):
            colnames = [colnames]

        res = [list(self.direct_refs[d]) for d in colnames]
        return ". ".join(sk.flatten(res))



