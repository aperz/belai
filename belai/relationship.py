"""
TODO:

with Pool(10) as p:
    res = p.map(enrichr_s, [smapp for smapp in mapp.groupby('ch')])
"""
import os
import re
import pandas as pd
import io

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS

from gseapy import enrichr, get_library_name
from belai import data
from belai import example
from belai import entity
from belai.util import memoize, get_configuration
#from belai.glove_model import glove_model; assign to var (long loading)

config = get_configuration()
from belai.config import CONFIG #config.py

def get_bow_background():
    """
    Get the globally most common words for all entity descriptions.
    TODO: Implement; current version is a dummy, returns a manually curated list.
    """
    return frozenset(['gene', 'enzyme', 'protein', 'results'])

class EntityList(list):
    """
    An object to discover and hold the relationships between `Entities`.
    TODO: The input to this class should be a list of names instead of a list
    of `Entity` objects.
    """

    def __init__(self, list_of_entities, description=None):
        self.add_entities(list_of_entities)
        #self.name = list_name
        self.description = description

    def add_entities(self, entity_list):
        """
        Add a new `Entity` to the list.
        """
        if not isinstance(entity_list, list):
            entity_list = [entity_list]
        for e in entity_list:
            if e.name in self.names():
                print('Entity already in list.')
                continue
            #TODO: add using names + type (per item! EntityList should be able to collect all sorts of things

            #TODO
            #print(type(e))
            #if isinstance(e, entity.Entity):
            self.append(e)
            #else:
            #    print(e, 'is not an Entity instance')

    def from_differential_table(self):
        pass

    def from_DESeq2(self):
        pass

    def names(self):
        """
        Print names of entities in the list.
        """
        return [e.name for e in self]

    def differential(self, key='lFC'):
        """
        """
        return [e.differential[key] for e in self]

    def bag_of_words(self, colnames="all"):
        '''
        Return a bag of words data frame for all entities in list.
        Uses `sklearn`.
        '''
        corpus = [e.corpus(colnames) for e in self]
        #vectorizer = TfidfVectorizer(
        #                stop_words=ENGLISH_STOP_WORDS.union(data.biomart['stop_words']),
        #                smooth_idf = False, use_idf = True, sublinear_tf=False,
        #                min_df = 2, max_df = 1.0, max_features = None,
        #                )
        vectorizer = CountVectorizer(stop_words=ENGLISH_STOP_WORDS.union(get_bow_background()))
        X = vectorizer.fit_transform(corpus)

        #return X,vectorizer
        X = pd.DataFrame(X.toarray(),
                columns = vectorizer.get_feature_names(),
                index = self.names(),
                )

        return X

    def _process_bow(self, colnames="all", weighting = "magnify"):
        '''
        Prioritization:

        The importance of an annotation should be:
        - [x] inversely prop ro the number of hits of this annotation (TF) (GO term name.value_counts())
        - [x] intersection of other annotation - the more 'docs' have it, the MORE important (DF)
        - [ ] TODO: subtract background words for all genes - the less the fraction of total, the more important the annotation (weights are a param in config)
        - maybe boolean? Te number of matches in doc doesn't really matter much
        So ITF-DF-background?
        Introduce weights for the above in the config?

        TODO:
        - word vectors (find words that are similar - cosine distance - to the word 'gene':
        https://radimrehurek.com/gensim/tut1.html#from-strings-to-vectors

        - Glove vectors
        http://nlp.stanford.edu/data/glove.6B.zip
        '''

        # Step 1: Remove certain words
        min_len = 4
        min_ndocs = 2

        X = self.bag_of_words(colnames)
        bX = X.applymap(lambda x: int(bool(x)))

        drop_words = []
        drop_words.extend(data.biomart['stop_words'])
        drop_words.extend(bX.sum(0).index[bX.sum(0) < min_ndocs])
        drop_words.extend([f for f in X.columns if len(f) < min_len])
        drop_words.extend([s for s in X.columns if not pd.isnull(re.match('^\d', s))])

        X.drop(drop_words, axis=1, errors = 'ignore', inplace = True)

        # Step 2: Weight the counts for the remaining words
        if weighting == "none":
            pass
        elif weighting == "magnify":
            X = X*X.sum(0)
            X = ( X.transpose()*X.sum(1) ).transpose()
        elif weighting == "complex":
            pass
        else:
            raise ValueError('Invalid "weighting" argument.')

        return X


    def common_concepts(self, n=10, colnames="all"):
        """
        Get important features for the `EntityList`, as selected by the bag-of-words.
        """
        X = self._process_bow(colnames = colnames)
        selected_features = X.sum().sort_values(ascending=False)[:n].index.tolist()

        return selected_features


class GeneList(EntityList):
    def __init__(self, list_of_entities):
        EntityList.__init__(self, list_of_entities=list_of_entities)


    def gsea_get_gene_sets(self):
        return get_library_name()

    def gsea(self, gene_sets):
        """
        Get the Gene Set Enrichment results for the selected libraries (gene sets).
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        #keep = ['Adjusted P-value', 'Combined Score', 'Gene_set', 'Genes', 'Old Adjusted P-value', 'Old P-value', 'Overlap', 'P-value', 'Term', 'Z-score']
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    def gsea_go_process(self, gene_sets = "GO_Biological_Process_2018"):
        """
        Get the Gene Set Enrichment results for the KEGG ontology.
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    def gsea_kegg(self, gene_sets = "KEGG_2019_Human"):
        """
        Get the Gene Set Enrichment results for the KEGG ontology.
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    def gsea_wikipathways(self, gene_sets = "WikiPathways_2019_Human"):
        """
        Get the Gene Set Enrichment results for the WikiPathways 2019 Human.
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    def gsea_disease(self, gene_sets = "OMIM_Disease"):
        """
        Get the Gene Set Enrichment results for OMIM.
        To get disease from GEO, use: Disease_Perturbations_from_GEO_down
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    def gsea_microbe(self, gene_sets = ["Microbe_Perturbations_from_GEO_down",
                                        "Microbe_Perturbations_from_GEO_down"]):
        """
        """
        enr = enrichr(self.names(), gene_sets=gene_sets)
        #keep = ['Adjusted P-value', 'Combined Score', 'Gene_set', 'Genes', 'Old Adjusted P-value', 'Old P-value', 'Overlap', 'P-value', 'Term', 'Z-score']
        keep = ['Term', 'Adjusted P-value', 'Overlap']
        return enr.results[keep].sort_values('Adjusted P-value')

    #def gsea_aging(self, )
    #def gsea_chemicals(self, )



